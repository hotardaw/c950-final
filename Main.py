# Name: Aaron Hotard
# Student ID: 011107379
import csv
import datetime
from builtins import ValueError
import TruckModule

from PackageModule import Package
from HashTable import HashMap

# Initialize hash table
package_hash_table = HashMap()

# Read the file of distance information
with open("CSV/Distances.csv", encoding="utf-8") as csvDistance:
    CSV_Distance = list(csv.reader(csvDistance))

# Read the file of address information
with open("CSV/Addresses.csv", encoding="utf-8") as csvAddress:
    CSV_Address = list(csv.reader(csvAddress))

# Read the file of package information
with open("CSV/Packages.csv", encoding="utf-8") as csvPackage:
    CSV_Package = list(csv.reader(csvPackage))


# Method to extract addresses from string literal of address
def get_address(obj):
    for row in CSV_Address:
        if obj.address in row[2]:
            return int(row[0])


# Create package object(s) from Package_File.csv
# Insert package object(s) into package_hash_table
def load_package_data():
    with open("CSV/Packages.csv", encoding="utf-8") as package_data:
        package_data = csv.reader(package_data)
        for package in package_data:
            (
                packageID,
                packageAddress,
                packageCity,
                packageState,
                packageZipcode,
                packageDeadline,
                packageWeight,
            ) = map(str.strip, package[:7])
            # The `map(str.strip, package[:7])`` is used to remove the whitespace from fields read from CSV

            package_object = Package(
                int(packageID),
                packageAddress,
                packageCity,
                packageState,
                packageZipcode,
                packageDeadline,
                packageWeight,
                "At hub",
            )

            package_hash_table.insert(int(packageID), package_object)


# Load packages into hash table
load_package_data()


# Find distance between two package addresses
def distance_between(address1, address2):
    # Check if either of the addresses is None
    if address1 is None or address2 is None:
        return 0.0  # Return a default distance value or raise an error if appropriate

    distance = CSV_Distance[address1][address2]
    if distance == "":
        distance = CSV_Distance[address2][address1]
        # Distance data can be access via CSV_Distance[address1][address2]
    return float(distance)


total_mileage = 0  # Initialize the global mileage variable for all trucks. It's used in deliver_packages below:


def deliver_packages(truck):
    global total_mileage  # Access the global mileage variable

    # Iterate through package IDs until empty
    while len(truck.packages) > 0:
        next_package_id = None
        next_address = float("inf")

        for package_id in truck.packages:
            package = package_hash_table.search(package_id)

            if distance_between(get_address(truck), get_address(package)) <= next_address:
                next_address = distance_between(get_address(truck), get_address(package))
                next_package_id = package_id

        next_package = package_hash_table.search(next_package_id)

        # Remove next nearest package from truck packages dictionary
        truck.packages.remove(next_package_id)

        # Add distance to next_address to total_mileage & update truck's current address to current delivery:
        total_mileage += next_address
        truck.address = next_package.address

        # Find the time cost for the next nearest package & update it
        truck.time += datetime.timedelta(hours=next_address / 18)
        next_package.delivery_time = truck.time
        next_package.departure_time = truck.departure_time


# Function to update package #9's address:
def update_package_address(package_id, new_address, new_city, new_state, new_zip, new_deadline):
    # Update the package information in the hash table
    package = package_hash_table.search(package_id)
    package.address = new_address
    package.city = new_city
    package.state = new_state
    package.zipcode = new_zip
    package.deadline = new_deadline

    # Update the package information in the CSV file
    with open("CSV/Packages.csv", "r", newline="", encoding="utf-8") as file:
        reader = csv.reader(file)
        lines = list(reader)

    with open("CSV/Packages.csv", "w", newline="", encoding="utf-8") as file:
        writer = csv.writer(file)
        for line in lines:
            if line[0] == str(package_id):
                line[1] = new_address
                line[2] = new_city
                line[3] = new_state
                line[4] = new_zip
                line[5] = new_deadline
            writer.writerow(line)


# Load trucks, and make truck_3 wait until truck_1 or truck_2 finishes
deliver_packages(TruckModule.truck_1)
deliver_packages(TruckModule.truck_2)

TruckModule.truck_3.departure_time = min(TruckModule.truck_1.time, TruckModule.truck_2.time)
deliver_packages(TruckModule.truck_3)


# The console UI
class Main:
    print(
        """
====================================================================================
| = |                                                                          | = | 
| = |             Aaron Hotard's C950 Task 2: WGUPS Routing Program            | = | 
| = |                                                                          | = | 
===================================================================================="""
    )
    print(f"Total mileage for all completed routes: {round(total_mileage, 1)} miles")

    # Take a time input from user to check all packages:
    try:
        # Take a time input from user
        user_time_input = input("Enter a time to check deliveries. Use format HH:MM. Input: ")
        user_hours, user_minutes = map(int, user_time_input.split(":"))
        hh_mm_time = datetime.timedelta(hours=user_hours, minutes=user_minutes)

        # Calculate the target time (10:20 AM)
        target_hours, target_minutes = 10, 20
        target_time = datetime.time(hour=target_hours, minute=target_minutes)

        # Construct datetime objects for user input time and target time
        user_datetime = datetime.datetime.combine(datetime.date.today(), datetime.time(hour=user_hours, minute=user_minutes))
        target_datetime = datetime.datetime.combine(datetime.date.today(), target_time)

        if user_datetime >= target_datetime:
            update_package_address(9, "410 S. State St.", "Salt Lake City", "UT", "84111", "EOD")

        # Take user input to decide whether to check one or all packages
        user_selection_input = input("Enter 'one' or 'all' to check package status: ")

        if user_selection_input == "one":
            # Logic to check the status of a single package
            single_package_input = int(input("Enter package ID: "))  # Replace with actual input logic
            package = package_hash_table.search(int(single_package_input))
            package.update_delivery_status(hh_mm_time)
            print(str(package))

        elif user_selection_input == "all":
            # Logic to check the status of all packages
            for package_key in sorted(package_hash_table.return_keys()):
                package = package_hash_table.search(package_key)
                if hh_mm_time >= package.departure_time:
                    package.update_delivery_status(hh_mm_time)
                print(str(package))

        else:
            print("Invalid input. Closing program.")

    except ValueError:
        print("Entry invalid. Closing program.")
        exit()
