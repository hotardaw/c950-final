import datetime


# Create freight truck class
class Truck:
    def __init__(self, capacity, speed, load, packages, address, departure_time):
        self.capacity = capacity
        self.speed = speed
        self.load = load
        self.packages = packages
        self.address = address
        self.departure_time = departure_time
        self.time = departure_time

    def __str__(self):
        return "%s, %s, %s, %s, %s, %s" % (
            self.capacity,
            self.speed,
            self.load,
            self.packages,
            self.address,
            self.departure_time,
        )


def create_truck(packages, departure_time):
    return Truck(16, 18, None, packages, "4001 South 700 East", departure_time)


# Initialize trucks using the function
truck_1 = create_truck([1, 13, 14, 15, 16, 19, 20, 29, 30, 31, 34, 37, 40], datetime.timedelta(hours=8))
truck_2 = create_truck([3, 9, 12, 18, 21, 22, 23, 24, 26, 27, 35, 36, 38, 39], datetime.timedelta(hours=10, minutes=20))
truck_3 = create_truck([2, 4, 5, 6, 7, 8, 10, 11, 17, 25, 28, 32, 33], datetime.timedelta(hours=9, minutes=5))
