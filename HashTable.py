# Create Hash Map Class:
class HashMap:
    # Constructor initializes hash table (as a list of lists) with capacity of 20:
    def __init__(self, capacity=10):
        self.hash_table = []  # Create attribute to hold the hash table
        for _ in range(capacity):  # Create 20 sublists inside of self.list
            self.hash_table.append([])  # Each sublist is empty for now

    # Get bucket index - used to make code more readable in subsequent Functions
    def get_bucket_index(self, key):
        return hash(key) % len(self.hash_table)

    # Inserts key-value pairs into the list:
    def insert(self, key, value):
        # Calculate the `bucket_index` where the value should be stored
        bucket_index = self.get_bucket_index(key)
        # Retrieve hash_table associated w/ calculated `bucket_index`
        selected_bucket = self.hash_table[bucket_index]

        for kvpair in selected_bucket:
            if kvpair[0] == key:
                kvpair[1] = value
                return True

        selected_bucket.append([key, value])
        return True

    # Remove item from hash table:
    def remove(self, key):
        # Calculate the `bucket_index` where the value should be stored
        bucket_index = self.get_bucket_index(key)
        # Retrieve hash_table associated w/ calculated `bucket_index`
        selected_bucket = self.hash_table[bucket_index]

        # Remove the selected_bucket
        for pair in selected_bucket:
            if key == pair[0]:
                selected_bucket.remove(pair)
                return

    # Look up items in the hash table:
    def search(self, key):
        # Calculate the `bucket_index` where the value should be stored
        bucket_index = self.get_bucket_index(key)
        # Retrieve hash_table associated w/ calculated `bucket_index`
        selected_bucket = self.hash_table[bucket_index]

        for pair in selected_bucket:
            if key == pair[0]:
                return pair[1]
        return None

    def return_keys(self):
        all_keys = []
        for bucket in self.hash_table:
            for pair in bucket:
                all_keys.append(pair[0])  # Append the key from each key-value pair
        return all_keys
